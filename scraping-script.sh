#!/bin/bash

#IMPORTANT! THIS SCRIPT DISPLAYS TIME IN Z (ZULU/UTC). NORWAY (CET) AND UTC ARE 1 HOUR APART.

#LATITUDE AND LONGITUDE FOR DIFFERENT CITIES

declare -A LAT LON;

LAT[gjoevik]="60.8941"; LON[gjoevik]="10.5001"
LAT[oslo]="59.9139"; LON[oslo]="10.7522"
LAT[trondheim]="63.4305"; LON[trondheim]="10.3951"
LAT[bergen]="60.3913"; LON[bergen]="5.3221"
LAT[tromsoe]="69.6492"; LON[tromsoe]="18.9553"
LAT[kristiansand]="58.1599"; LON[kristiansand]="8.0182"

#OPTIONAL FEATURE 1: IF/ELSE STATEMENT THAT MAKES THE VARIABLE (EVEN_DAYS_ONLY) THE VALUE (KRISTIANSTAND) ON EVEN DAYS

DAY=$(date +%d)

if (( DAY % 2 == 0 ));

then even_days_only='kristiansand';

fi

#CREATES A DIRECTORY WITH THE CURRENT DATE + HOUR (CET TIME) AS THE DIRECTORY NAME (WITH THE DATE FORMAT SHOWN IN THE EXAMPLE OUTPUT)

DIRECTORY=$(date +"%Y-%m-%d-%H00")
mkdir -p "scraped-weather"/$DIRECTORY

#FOR LOOP THAT CREATES XML FILES FOR DIFFERENT CITIES AND RUNS THE COMMANDS, PUTTING THE OUTPUT IN EACH .TXT FILE

for location in gjoevik oslo trondheim bergen tromsoe ${even_days_only};
do
        curl -o "${location}.xml" -s "https://api.met.no/weatherapi/locationforecast/2.0/classic?lat=${LAT[$location]}&lon=${LON[$location]}"
        CURRENTDATE=$(date +"%Y-%m-%d %H:%m")
        CURRENTHOUR=$(grep 'termin' "${location}.xml" | grep -E -o [0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}Z | head -n1)
        ONEAFTER=$(date -u -d "$CURRENTHOUR +1 hour" +%FT%H:%M:%SZ)
        TWOAFTER=$(date -u -d "$CURRENTHOUR +2 hour" +%FT%H:%M:%SZ)
        echo "${location^}" >> scraped-weather/$DIRECTORY/${location}.txt
        grep -A 19 -E "from=\"$CURRENTHOUR\"\s*to=\"$CURRENTHOUR\"" "${location}.xml" | grep temperature | grep -E -o -e '-?[0-9]+\.[0-9]+' >> scraped-weather/$DIRECTORY/${location}.txt
        grep -A 19 -E "from=\"$ONEAFTER\"\s*to=\"$ONEAFTER\"" "${location}.xml" | grep temperature | grep -E -o -e '-?[0-9]+\.[0-9]+' >> scraped-weather/$DIRECTORY/${location}.txt
        grep -A 19 -E "from=\"$TWOAFTER\"\s*to=\"$TWOAFTER\"" "${location}.xml" | grep temperature | grep -E -o -e '-?[0-9]+\.[0-9]+' >> scraped-weather/$DIRECTORY/${location}.txt
        grep -A 19 -E "from=\"$CURRENTHOUR\"\s*to=\"$CURRENTHOUR\"" "${location}.xml" | grep -E -o 'code=.*' | sed 's/code=["]//g ; s/symbol//g ; s/["></]//g' >> scraped-weather/$DIRECTORY/${location}.txt
        echo $CURRENTDATE >> scraped-weather/$DIRECTORY/${location}.txt
	#OPTIONAL FEATURE 2: GREPING AND INCLUDING HUMIDITY IN THE SCRIPT  
	grep -A 19 -E "from=\"$CURRENTHOUR\"\s*to=\"$CURRENTHOUR\"" "${location}.xml" | grep humidity | grep -E -o -e '[0-9]+\.[0-9]' >> scraped-weather/$DIRECTORY/${location}.txt
done

#THIS COMMAND REMOVES THE .XML FILES

rm *.xml
