#!/bin/bash

#IF/ELSE STATEMENT THAT FORCES YOU TO RUN THE SCRIPT IN THE RIGHT DIRECTORY, ELSE YOU WILL GET A MESSAGE

if [[ -d "./scraped-weather" ]]; then
  STORED_PATH=$(pwd);
else
  echo "You're running this script while being in a wrong directory.";
  exit 1;
fi

cd "./scraped-weather" || exit 2;

#A FOR LOOP THAT CREATES VARIABLES NEEDED FOR THE HTML PAGES AND OUTPUTS THE RIGHT DATA

for datetime in *; do

        initial_forecast_time="$(date -d "${datetime: -4}" +'%H:%M')"
	initial_forecast_timeONEHR="$(date -d "${datetime: -4} +1 hour" +'%H:%M')"
	initial_forecast_timeTWOHR="$(date -d "${datetime: -4} +2 hour" +'%H:%M')"
	mkdir -p $STORED_PATH/cities/$datetime

	for city in $STORED_PATH/scraped-weather/$datetime/*.txt; do
  		nameofcity=$(head -n 1 "$city")
		tempone=$(head -n 2 "$city" | tail -n 1)
		temptwo=$(head -n 3 "$city" | tail -n 1)
		tempthree=$(head -n 4 "$city" | tail -n 1)
		precipitation=$(head -n 5 "$city" | tail -n 1)
		currentdatetime=$(head -n 6 "$city" | tail -n 1)
		humidity=$(tail -n 1 "$city")

		cat <<-EOF > $STORED_PATH/cities/$datetime/${nameofcity,}.html
		<!DOCTYPE html> 
		<html> 
		<head><title>Weather forecast for ${nameofcity}</title></head> 
		<body> 
		<h1>Weather forecast for ${nameofcity}</h1>
		<p>Temperatures:</p>
		<ul>
		<li>${initial_forecast_time}: ${tempone}</li>
		<li>${initial_forecast_timeONEHR}: ${temptwo}</li>
		<li>${initial_forecast_timeTWOHR}: ${tempthree}</li>
		</ul>
		<p>Forecast: ${precipitation}</p>
		<p>Fetched on ${currentdatetime}</p>
		<a href="../../index.html">Weather forecast for Norwegian cities.</a>
		</body>
		</html>
EOF

	done


done
