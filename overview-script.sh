#!/bin/bash

#IF/ELSE STATEMENT THAT FORCES YOU TO RUN THE SCRIPT IN THE RIGHT DIRECTORY, ELSE YOU WILL GET A MESSAGE

if [[ -d "./scraped-weather" ]]; then
  STORED_PATH=$(pwd);
else
  echo "You're running this script while being in a wrong directory.";
  exit 1;
fi

cd "./scraped-weather" || exit 2;

#FOR LOOP AND IF/ELSE STATEMENT THAT GETS THE RIGHT TIMESTAMP AND CREATES THE VARIABLES NEEDED FOR INDEX.HTML
newest=0;
for subdir in *; do
 currently_checked=$(date -d "${subdir%-*} ${subdir##*-}" +%s);

  	if [ $currently_checked -gt $newest ]; then
    		newest=$currently_checked
    		newest_name=$subdir
	fi
done

#VARIABLE WITH THE RIGHT DATE FORMAT AND THE HTML STRUCTURE

HTMLdate=$(date -d @"$newest" +'%Y-%d-%m %H:00')

cat <<-EOF > $STORED_PATH/index.html
<!DOCTYPE html> 
<head>
<title>Weather forecasts</title>
</head>
<body>
<p>Forecasts updated at $HTMLdate.</p>
<ul> 
EOF

#FOR LOOP THAT CREATES THE LISTS, LINKS TO THE FORECASTS FOR EACH CITY
#OPTIONAL FEATURE 3: DISPLAYS THE FORECAST AND TEMPERATURE NEXT TO THE LINKS, USING VARIABLES FROM PAGES-SCRIPT

for city in $STORED_PATH/scraped-weather/$newest_name/*.txt; do
        nameofcity=$(head -n 1 "$city")
        tempone=$(head -n 2 "$city" | tail -n 1)
        temptwo=$(head -n 3 "$city" | tail -n 1)
        tempthree=$(head -n 4 "$city" | tail -n 1)
        precipitation=$(head -n 5 "$city" | tail -n 1)
        echo "<li><a href=\"cities/${newest_name}/${nameofcity,}.html\"/>${nameofcity}</a>: (${precipitation}) ${tempone}, ${temptwo}, ${tempthree}</li>" >> $STORED_PATH/index.html
done

#TO INCLUDE THE CLOSING TAGS FOR THE UL AND BODY ELEMENTS IN HTML

cat <<-EOF >> $STORED_PATH/index.html
</ul>
</body>
EOF

