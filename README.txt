In this project I have created the necessary scripts (scraping, pages- and overview-script) and implemented 3 optional features (single stars). I have also set up a cronjob to run the scripts every 6 hours and set up NGINX to display the html files on a specified server name.

The scraping-script fetches the weather forcast from the API for the different cities using LATITUDE and LONGITUDE and extracts the necessary data which is temperature, precipitation and the optional feature which I have implemented (humidity)
OPTIONAL FEATURES IMPLEMENTED IN SCRAPING-SCRIPT: 
1. On even days also scrape data for Kristiansand.
2. Also retrieve information about humidity for each city and add it as a seventh line in the information files. 

The pages-script reads and creates the necessary HTML files for each city, displaying the meteorological data recieved by running the scraping-script. 
OPTIONAL FEATURES IMPLEMENTED IN PAGES-SCRIPT:
NONE

The overview-script creates a HTML page which shows the different cities as links. Behind the list elements (links) the optional feature is implemented where you can see the current forecast followed by current temperature and temperature after 1 and 2 hours. (same as in example output)
OPTIONAL FEATURES IMPLEMENTED IN OVERVIEW-SCRIPT:
1. Show the current temperature and forecast after the city name.

The NGINX configuration allows the HTML pages to be displayed on a internet browser by entering the path to the html pages and specifying the server name to be searched. (More info in the nginx.conf file)

The crontab configuration makes the scripts run 1 minute after eachother once every 6 hours. (More info in the crontab.conf file)
